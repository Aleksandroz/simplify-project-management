require 'test_helper'

class NavControllerTest < ActionController::TestCase
  test "should get projects" do
    get :projects
    assert_response :success
  end

  test "should get canvas" do
    get :canvas
    assert_response :success
  end

  test "should get details" do
    get :details
    assert_response :success
  end

  test "should get scope" do
    get :scope
    assert_response :success
  end

  test "should get cost" do
    get :cost
    assert_response :success
  end

  test "should get schedule" do
    get :schedule
    assert_response :success
  end

  test "should get communications" do
    get :communications
    assert_response :success
  end

  test "should get risk" do
    get :risk
    assert_response :success
  end

  test "should get procurement" do
    get :procurement
    assert_response :success
  end

  test "should get quality" do
    get :quality
    assert_response :success
  end

  test "should get resources" do
    get :resources
    assert_response :success
  end

end
