require 'test_helper'

class StakeholderTasksControllerTest < ActionController::TestCase
  setup do
    @stakeholder_task = stakeholder_tasks(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:stakeholder_tasks)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create stakeholder_task" do
    assert_difference('StakeholderTask.count') do
      post :create, stakeholder_task: @stakeholder_task.attributes
    end

    assert_redirected_to stakeholder_task_path(assigns(:stakeholder_task))
  end

  test "should show stakeholder_task" do
    get :show, id: @stakeholder_task
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @stakeholder_task
    assert_response :success
  end

  test "should update stakeholder_task" do
    put :update, id: @stakeholder_task, stakeholder_task: @stakeholder_task.attributes
    assert_redirected_to stakeholder_task_path(assigns(:stakeholder_task))
  end

  test "should destroy stakeholder_task" do
    assert_difference('StakeholderTask.count', -1) do
      delete :destroy, id: @stakeholder_task
    end

    assert_redirected_to stakeholder_tasks_path
  end
end
