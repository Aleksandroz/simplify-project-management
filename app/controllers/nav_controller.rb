class NavController < ApplicationController
 
  def projects
    @projects = Project.order(:title)

    respond_to do |format|
      format.html # projects.html.erb
      format.json { render json: @projects }
    end
  end

  def canvas
    @project = Project.find(params[:project_id])

    respond_to do |format|
      format.html # canvas.html.erb
      format.json { render json: @project }
    end
  end

  def charter
    @project = Project.find(params[:project_id])
    @tasks = @project.tasks
    respond_to do |format|
      format.html # charter.html.erb
      format.json { render json: @project }
    end
  end

  def details
    @project = Project.find(params[:project_id])

    respond_to do |format|
      format.html # details.html.erb
      format.json { render json: @project }
    end
  end

  def scope
    @project = Project.find(params[:project_id])
    @task_row_number = 0

    respond_to do |format|
      format.html # scope.html.erb
      format.json { render json: @project }
    end
  end

  def cost
    @project = Project.find(params[:project_id])

    respond_to do |format|
      format.html # cost.html.erb
      format.json { render json: @project }
    end
  end

  def schedule
    @project = Project.find(params[:project_id])

    respond_to do |format|
      format.html # schedule.html.erb
      format.json { render json: @project }
    end
  end

  def communications
    @project = Project.find(params[:project_id])

    respond_to do |format|
      format.html # communications.html.erb
      format.json { render json: @project }
    end
  end

  def risk
    @project = Project.find(params[:project_id])

    respond_to do |format|
      format.html # risk.html.erb
      format.json { render json: @project }
    end
  end

  def procurement
    @project = Project.find(params[:project_id])

    respond_to do |format|
      format.html # procurement.html.erb
      format.json { render json: @project }
    end
  end

  def quality
  end

  def resources
    @project = Project.find(params[:project_id])

    respond_to do |format|
      format.html # resources.html.erb
      format.json { render json: @project }
    end
  end
  
  def delete_project
    @project = Project.find(params[:project_id])
    @project.destroy

    respond_to do |format|
      format.html { redirect_to nav_projects_path }
      format.json { head :no_content }
    end
  end
  
  def new_project
    @project = Project.new(title: "New Project")
    @project.save
    @task = Task.new(name: "Project Header Task", project_id: @project.id, level: 0, parent_task_id: -1)
    @task.save

    respond_to do |format|
      format.html { redirect_to nav_details_path(project_id: @project) }
 #      format.html { redirect_to nav_projects_path }
      format.json { render json: @project }
    end
  end
  
  def update_project
    @project = Project.find(params[:project_id])
    @current_page = params[:current_page]
    respond_to do |format|
      if @project.update_attributes(params[:project])
        if @current_page == 'details'
          format.html { redirect_to nav_details_path(project_id: @project) }    #  format.html { redirect_to nav_details_path(project_id: @project), notice: 'Project was successfully updated.' }
        elsif @current_page == 'scope'
          format.html { redirect_to nav_scope_path(project_id: @project) }
        elsif @current_page == 'cost'
          format.html { redirect_to nav_cost_path(project_id: @project) }
        elsif @current_page == 'procurement'
          format.html { redirect_to nav_procurement_path(project_id: @project) }
        elsif @current_page == 'schedule'
          format.html { redirect_to nav_schedule_path(project_id: @project) }
        end
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  def delete_task
    @project = Project.find(params[:project_id])
    @task = Task.find(params[:task_id])
    @sibling_tasks = Project.find(params[:project_id]).tasks.where("parent_task_id = ?", @task.parent_task_id)

    @child_tasks = Task.where("parent_task_id = ?", params[:task_id])
    @child_tasks.each do |child|
 #     delete_task(:project_id => @project, :task_id => child)     # cannot pass arguments again - reading from same hash. need to break out into helper method!!!!!! need to destroy or reassign children.
    end

    @task.destroy

    respond_to do |format|
      format.html { redirect_to nav_scope_path(project_id: @project) }
      format.json { head :no_content }
    end
  end
  
  def new_task
    @project = Project.find(params[:project_id])
    @header_task = Task.where("project_id = ? AND level = ?",@project.id, 0).first

    @first_level_tasks = Task.where("parent_task_id = ?",@header_task.id)
    @index = 0
    @first_level_tasks.each do |task|
      if task.level > @index
        @index = task.level
      end
    end
    @index += 1

    @task = Task.new(name: "New Task", project_id: @project.id, level: @index, parent_task_id: @header_task.id)
    @task.save

    respond_to do |format|
      format.html { redirect_to nav_scope_path(project_id: @project) }
      format.json { render json: @task }
    end
  end

  def update_tasks
    @project = Project.find(params[:project_id])
    respond_to do |format|
      if @project.update_attributes(params[:project])
 #       @project.tasks.each do |task|
 #         if task.level == 0
 #           set_task_level(task_id: task.id, level: 0)
 #         end
 #       end
        format.html { redirect_to nav_scope_path(project_id: @project) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  def delete_stakeholder
    @project = Project.find(params[:project_id])
    @current_page = params[:current_page]
    @stakeholder = Stakeholder.find(params[:stakeholder_id])
    @stakeholder.destroy

    respond_to do |format|
        if @current_page == 'communications'
          format.html { redirect_to nav_communications_path(project_id: @project) }    
        elsif @current_page == 'resources'
          format.html { redirect_to nav_resources_path(project_id: @project) }
        end
      format.json { head :no_content }
    end
  end
  
  def new_stakeholder
    @project = Project.find(params[:project_id])
    @current_page = params[:current_page]
    @stakeholder = Stakeholder.new(name: "New Stakeholder", project_id: @project.id)
    @stakeholder.save

    respond_to do |format|
        if @current_page == 'communications'
          format.html { redirect_to nav_communications_path(project_id: @project) }    
        elsif @current_page == 'resources'
          format.html { redirect_to nav_resources_path(project_id: @project) }
        end
      format.json { render json: @stakeholder }
    end
  end

  def update_stakeholders
    @project = Project.find(params[:project_id])
    @current_page = params[:current_page]
    respond_to do |format|
      if @project.update_attributes(params[:project])
        if @current_page == 'communications'
          format.html { redirect_to nav_communications_path(project_id: @project) }    
        elsif @current_page == 'resources'
          format.html { redirect_to nav_resources_path(project_id: @project) }
        end
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  def delete_risk
    @project = Project.find(params[:project_id])
    @risk = Risk.find(params[:risk_id])
    @risk.destroy

    respond_to do |format|
      format.html { redirect_to nav_risk_path(project_id: @project) }
      format.json { head :no_content }
    end
  end

  def new_risk
    @project = Project.find(params[:project_id])
    @risk = Risk.new(name: "New Risk", project_id: @project.id)
    @risk.save

    respond_to do |format|
      format.html { redirect_to nav_risk_path(project_id: @project) }
      format.json { render json: @stakeholder }
    end
  end

  def update_risks
    @project = Project.find(params[:project_id])
    respond_to do |format|
      if @project.update_attributes(params[:project])
        format.html { redirect_to nav_risk_path(project_id: @project) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

end
