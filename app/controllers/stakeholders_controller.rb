class StakeholdersController < ApplicationController
  before_filter :get_project
  before_filter :set_page_image
  
  # GET /stakeholders
  # GET /stakeholders.json
  def index
    if params[:view] == 'resources'
      @stakeholders = Project.resources(params[:project_id])
    else
      @stakeholders = Stakeholder.find_all_by_project_id(params[:project_id])
    end
    
    @stakeholder = @project.stakeholders.build # for new stakeholder

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @stakeholders }
    end
  end

  # GET /stakeholders/1
  # GET /stakeholders/1.json
  def show
    @stakeholder = Stakeholder.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @stakeholder }
    end
  end

  # GET /stakeholders/new
  # GET /stakeholders/new.json
  def new
    @stakeholder = @project.stakeholders.build

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @stakeholder }
    end
  end

  # GET /stakeholders/1/edit
  def edit
    @stakeholder = Stakeholder.find(params[:id])
  end

  # POST /stakeholders
  # POST /stakeholders.json
  def create
    @stakeholder = @project.stakeholders.build(params[:stakeholder])

    respond_to do |format|
      if @stakeholder.save
        format.html { redirect_to project_stakeholders_path(@project, :view => 'stakeholders'), notice: 'Stakeholder was successfully created.' }
        format.json { render json: @stakeholder, status: :created, location: @stakeholder }
      else
        format.html { render action: "new" }
        format.json { render json: @stakeholder.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /stakeholders/1
  # PUT /stakeholders/1.json
  def update
    @stakeholder = Stakeholder.find(params[:id])

    respond_to do |format|
      if @stakeholder.update_attributes(params[:stakeholder])
        format.html { redirect_to project_stakeholders_path(@project, :view => 'stakeholders'), notice: 'Stakeholder was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @stakeholder.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stakeholders/1
  # DELETE /stakeholders/1.json
  def destroy
    @stakeholder = Stakeholder.find(params[:id])
    @stakeholder.destroy

    respond_to do |format|
      format.html { redirect_to stakeholders_url(:project_id => @project) }
      format.json { head :no_content }
    end
  end
  
  private
  def set_page_image
    @page_image = 'stake.jpg'
  end
  
  def get_project
    @project = Project.find(params[:project_id])
  end
end
