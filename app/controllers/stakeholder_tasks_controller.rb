class StakeholderTasksController < ApplicationController
  # GET /stakeholder_tasks
  # GET /stakeholder_tasks.json
  def index
    @stakeholder_tasks = StakeholderTask.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @stakeholder_tasks }
    end
  end

  # GET /stakeholder_tasks/1
  # GET /stakeholder_tasks/1.json
  def show
    @stakeholder_task = StakeholderTask.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @stakeholder_task }
    end
  end

  # GET /stakeholder_tasks/new
  # GET /stakeholder_tasks/new.json
  def new
    @stakeholder_task = StakeholderTask.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @stakeholder_task }
    end
  end

  # GET /stakeholder_tasks/1/edit
  def edit
    @stakeholder_task = StakeholderTask.find(params[:id])
  end

  # POST /stakeholder_tasks
  # POST /stakeholder_tasks.json
  def create
    @stakeholder_task = StakeholderTask.new(params[:stakeholder_task])

    respond_to do |format|
      if @stakeholder_task.save
        format.html { redirect_to @stakeholder_task, notice: 'Stakeholder task was successfully created.' }
        format.json { render json: @stakeholder_task, status: :created, location: @stakeholder_task }
      else
        format.html { render action: "new" }
        format.json { render json: @stakeholder_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /stakeholder_tasks/1
  # PUT /stakeholder_tasks/1.json
  def update
    @stakeholder_task = StakeholderTask.find(params[:id])

    respond_to do |format|
      if @stakeholder_task.update_attributes(params[:stakeholder_task])
        format.html { redirect_to @stakeholder_task, notice: 'Stakeholder task was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @stakeholder_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stakeholder_tasks/1
  # DELETE /stakeholder_tasks/1.json
  def destroy
    @stakeholder_task = StakeholderTask.find(params[:id])
    @stakeholder_task.destroy

    respond_to do |format|
      format.html { redirect_to stakeholder_tasks_url }
      format.json { head :no_content }
    end
  end
end
