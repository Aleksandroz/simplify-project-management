class ApplicationController < ActionController::Base
  protect_from_forgery
  
  helper_method :get_project
  helper :all
 
  private
  def get_project
    @project = Project.find(params[:project_id])
  end
  
end
