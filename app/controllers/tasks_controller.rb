class TasksController < ApplicationController
  before_filter :get_project
  before_filter :set_page_image
  
  # GET /tasks
  # GET /tasks.json
  def index
    @tasks = @project.tasks  #Task.find_all_by_project_id(params[:project_id]) #Task.all
    @task = @project.tasks.build # for new task
    
    @title_length = 20
    @description_length = 23
    @milestone_description_length = 30

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @tasks }
    end
  end

  # GET /tasks/1
  # GET /tasks/1.json
  def show
    @task = Task.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @task }
    end
  end

  # GET /tasks/new
  # GET /tasks/new.json
  def new
    @task = @project.tasks.build

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @task }
    end
  end

  # GET /tasks/1/edit
  def edit
    @task = Task.find(params[:id])
  end

  # POST /tasks
  # POST /tasks.json
  def create
    @task =  @project.tasks.build(params[:task])

    respond_to do |format|
      if @task.save
        format.html { redirect_to project_tasks_path(@project), notice: 'Task was successfully created.' }
        format.json { render json: @task, status: :created, location: @task }
      else
        format.html { render action: "new" }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /tasks/1
  # PUT /tasks/1.json
  def update
    @task = Task.find(params[:id])

    respond_to do |format|
      if @task.update_attributes(params[:task])
        format.html { redirect_to project_tasks_path(@project), notice: 'Task was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tasks/1
  # DELETE /tasks/1.json
  def destroy
    @task = Task.find(params[:id])
    @task.destroy

    respond_to do |format|
      format.html { redirect_to project_tasks_path(@project), notice: 'Task was successfully deleted.' }
      format.json { head :no_content }
    end
  end
  
    
  def savesort
    #neworder = params[:list]
    neworder = JSON.parse(params[:set])
    prev_item = nil
    
    #redirect_to project_tasks_path(@project)
    
    neworder.each_with_index do |item,v|
      dbitem = Task.find(item['id'].to_i)
      prev_item.nil? ? dbitem.move_to_root : dbitem.move_to_right_of(prev_item)
      sort_children(item, dbitem) unless item['children'].nil?
      prev_item = dbitem
    end
    Task.rebuild!
    
    #respond_to do |format|
     #   format.xml { render xml: @tasks }
    #end
    
    #render :partial => 'tasks_list' , :notice => 'Tasks successfully updated'
    redirect_to project_tasks_path(@project), :notice => 'Tasks successfully updated'
  end
  
  
  private
  def set_page_image
    @page_image = 'tasks.jpg'
  end
  
  def get_project
    @project = Project.find(params[:project_id])
  end
  
    def sort_children(element,dbitem)
    prevchild = nil
    element['children'].each do |child|
      childitem = Task.find(child['id'])
      prevchild.nil? ? childitem.move_to_child_of(dbitem) : childitem.move_to_right_of(prevchild)
      sort_children(child, childitem) unless child['children'].nil?
      prevchild = childitem
    end
  end
  
end
