class Task < ActiveRecord::Base
  acts_as_nested_set

  attr_protected :lft, :rgt
  
  belongs_to :project

  has_many :child_tasks, :class_name => "Task"
  belongs_to :parent_task, :class_name => "Task", :foreign_key => "parent_task_id"
  
  has_and_belongs_to_many :stakeholders
  
  def index
    
    if true#@index == nil
     if root?
        temp_index = 0
        Task.roots.each do |item|
            if item.project_id == project_id
              temp_index = temp_index + 1
            end
            
            if item.id == id 
                @index = (temp_index).to_s
            end
        end
     end
     if child?
        @parents = ancestors
        
        parent_index = 0
        
        @parents.each do |item|
            if item.id == parent_id
                parent_index = item.index
            end
        end
        
        @self_and_siblings = self_and_siblings
    
        @self_and_siblings.each_with_index do |item, index|
            if item.id == id
                @index = (parent_index).to_s + "." + (index + 1).to_s
            end
        end
     end
    end
    
    if @index == nil
        "Unknown"
    else
        @index
    end
  end
end
