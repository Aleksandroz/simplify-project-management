class Risk < ActiveRecord::Base
    belongs_to :project
    
  def rating
    if likelihood.nil? or consequence.nil?
      0
    else
      likelihood * consequence  
    end
  end
end
