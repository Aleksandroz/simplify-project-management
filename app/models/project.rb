class Project < ActiveRecord::Base
  has_many :tasks, :dependent => :destroy
  accepts_nested_attributes_for :tasks
  has_many :stakeholders
  accepts_nested_attributes_for :stakeholders
  has_many :risks, :dependent => :destroy
  accepts_nested_attributes_for :risks

  attr_accessor :budget_accuracy
  
  def budget_accuracy=(budget)
    amount = budget.to_f
    if budget == 0
      write_attribute(:budget_accuracy, (amount))
    else
      write_attribute(:budget_accuracy, (amount/100))
    end
  end
  
  def budget_accuracy_before_type_cast
  
    if super().nil?
      0
    else
      super()*100
      self[:budget_accuracy] * 100 #leaving bth methods for just in case
    end
      
    
  end

  def budget_accuracy
     if super().nil?
      0
    else
      super()*100 
      self[:budget_accuracy] * 100#leaving bth methods for just in case
    end
    
  end

  
  
  def resources
    Stakeholder.find(:all, :conditions => ["is_resource = ? and project_id = ?", true, id])
  end
  
  def self.resources(id)
    Stakeholder.find(:all, :conditions => ["is_resource = ? and project_id = ?", true, id])
  end
  
  def key_sponsors
    Stakeholder.find(:all, :conditions => ["project_id = ? and project_role IN (?)", id, ['sponsor', 'Sponsor', 'Project Sponsor', 'project sponsor' , 'Project sponsor']])
  end
  
  def key_managers
    Stakeholder.find(:all, :conditions => ["project_id = ? and project_role IN (?)", id, ['manager', 'Manager','Project Manager', 'project manager', 'Project manager']])
  end
  
  def key_stakeholders
    Stakeholder.find(:all, :conditions => ["project_id = ? and project_role IN (?)", id, ['sponsor', 'manager', 'Sponsor','Manager','Project Manager', 'project manager', 'Project manager', 'Project Sponsor', 'project sponsor' , 'Project sponsor']])
  end
  
  def milestones
    Task.find(:all, :conditions => ["is_milestone = ? and project_id = ?", true, id])
  end
  
end
