class Stakeholder < ActiveRecord::Base
  belongs_to :project
  has_and_belongs_to_many :tasks
  
  scope :resources, lambda { |entity| where("is_resource = ? and project_id = ?", true, id)}
  scope :milestones, lambda { |entity| where("is_milestone = ? and project_id = ?", true, id)}
  
  
end
