# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120722203000) do

  create_table "projects", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.string   "responsible_group"
    t.string   "cost_centre"
    t.decimal  "budget",               :precision => 12, :scale => 2
    t.decimal  "contingency",          :precision => 12, :scale => 2
    t.decimal  "budget_accuracy",      :precision => 4,  :scale => 3
    t.date     "start_date"
    t.date     "end_date"
    t.text     "procurement_strategy"
    t.datetime "created_at",                                          :null => false
    t.datetime "updated_at",                                          :null => false
    t.text     "purpose"
  end

  create_table "risks", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.decimal  "cost",        :precision => 10, :scale => 0
    t.integer  "delay"
    t.integer  "likelihood"
    t.integer  "consequence"
    t.text     "mitigation"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.integer  "project_id"
  end

  create_table "stakeholder_tasks", :force => true do |t|
    t.integer  "stakeholder_id"
    t.integer  "task_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "stakeholders", :force => true do |t|
    t.integer  "project_id"
    t.string   "name"
    t.string   "title"
    t.string   "department"
    t.string   "project_role"
    t.boolean  "is_resource"
    t.boolean  "is_approver"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.string   "stakeholder_group"
    t.integer  "frequency"
    t.string   "medium"
  end

  create_table "tasks", :force => true do |t|
    t.integer  "project_id"
    t.integer  "level"
    t.integer  "parent_task_id"
    t.string   "name"
    t.text     "description"
    t.datetime "start_date"
    t.datetime "end_date"
    t.boolean  "is_milestone",                                      :default => true
    t.text     "procurement_method"
    t.decimal  "budget",             :precision => 12, :scale => 2
    t.datetime "created_at",                                                          :null => false
    t.datetime "updated_at",                                                          :null => false
    t.string   "task_index"
    t.integer  "lft"
    t.integer  "rgt"
    t.integer  "parent_id"
  end

end
