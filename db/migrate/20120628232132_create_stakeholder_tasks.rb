class CreateStakeholderTasks < ActiveRecord::Migration
  def change
    create_table :stakeholder_tasks do |t|
      t.integer :stakeholder_id
      t.integer :task_id

      t.timestamps
    end
  end
end
