class CreateRisks < ActiveRecord::Migration
  def change
    create_table :risks do |t|
      t.string :name
      t.text :description
      t.decimal :cost
      t.integer :delay
      t.integer :likelihood
      t.integer :consequence
      t.text :mitigation
      t.datetime :created_at
      t.datetime :updated_at
      t.integer :project_id

      t.timestamps
    end
  end
end
