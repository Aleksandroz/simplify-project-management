class AddFreqAndMediumToStakeholders < ActiveRecord::Migration
  def change
    add_column :stakeholders, :frequency, :integer
    add_column :stakeholders, :medium, :string
  end
end
