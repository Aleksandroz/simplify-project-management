class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.integer :project_id
      t.integer :level
      t.integer :parent_task_id
      t.string :name
      t.text :description
      t.datetime :start_date
      t.datetime :end_date
      t.boolean :is_milestone
      t.text :procurement_method
      t.decimal :budget, precision: 12, scale: 2
      t.timestamps
      t.string :task_index

      
    end
  end
end
