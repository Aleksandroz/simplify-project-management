class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :title
      t.text :description
      t.string :responsible_group
      t.string :cost_centre
      t.decimal :budget, precision: 12, scale: 2
      t.decimal :contingency, precision: 12, scale: 2
      t.decimal :budget_accuracy, precision: 4, scale: 3
      t.date :start_date
      t.date :end_date
      t.text :procurement_strategy
      t.timestamps
      t.text :purpose

    end
  end
end
