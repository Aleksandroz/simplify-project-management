class ChangeDefaulValueForTasks < ActiveRecord::Migration
  def up
    change_column :tasks, :is_milestone, :boolean, :default => 1
  end

  def down
  end
end
