class CreateStakeholders < ActiveRecord::Migration
  def change
    create_table :stakeholders do |t|
      t.integer :project_id
      t.string :name
      t.string :title
      t.string :department
      t.string :project_role
      t.boolean :is_resource
      t.boolean :is_approver
      t.timestamps
      t.string :stakeholder_group

    end
  end
end
